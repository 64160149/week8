package com.saharat;

public class Rectangle {
    public int width;
    public int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int areas() {
        int areas = width*height;
        return areas;
    }

    public int lines() {
        int lines = (width*2)+(height*2);
        return lines;
    }
}
