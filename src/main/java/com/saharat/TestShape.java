package com.saharat;


public class TestShape {
    public static void main(String[] args) {
        Rectangle rect1 = new Rectangle(10, 5);
        Rectangle rect2 = new Rectangle(5, 3);
        Circle circle1 = new Circle(1);
        Circle circle2 = new Circle(2);
        Triangle triangle1 = new Triangle(5, 5, 6);

        System.out.println("Rectangle1 area = "+rect1.areas());
        System.out.println("Rectangle2 area = "+rect2.areas());
        System.out.println("Circle1 area = "+circle1.areas());
        System.out.println("Circle2 area = "+circle2.areas());
        System.out.println("Triangle1 area = "+triangle1.areas());
        System.out.println("Rectangle1 line = "+rect1.lines());
        System.out.println("Rectangle2 line = "+rect2.lines());
        System.out.println("Circle1 line = "+circle1.lines());
        System.out.println("Circle2 line = "+circle2.lines());
        System.out.println("Triangle1 line = "+triangle1.lines());
    }
}
