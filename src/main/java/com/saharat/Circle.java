package com.saharat;

public class Circle {
    public int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public  float areas() {
        float areas = (22 / 7) * (radius * radius);
        return areas;
    }

    public float lines() {
        float lines = (2 * (22 / 7)) * radius;
        return lines;
    }
}
