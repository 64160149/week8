package com.saharat;

import java.nio.channels.Pipe;

public class Map {
    public static int x_min;
    public static int y_min;
    public static int x_max;
    public static int y_max;

    public Map(int x_max, int y_max) {
        this.x_min = 0;
        this.y_min = 0;
        this.x_max = x_max;
        this.y_max = y_max;
    }

    public void Mapprint() {
        for (int y = Map.y_min; y < Map.y_max; y++) {
            for (int x = Map.x_min; x < Map.x_max + 1; x++) {
                if( y == y_max) {
                    System.out.print("\n");
                } else {
                    System.out.print("-");
                }
            }
            System.out.println(); 
        } 
    }
}
