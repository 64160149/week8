package com.saharat;

public class Triangle {
    public int side1;
    public int side2;
    public int side3;

    public Triangle(int side1, int side2, int side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public float areas() {
        float findS = (side1 + side2 + side3) / 2;
        float findAll = (findS-side1)*(findS-side2)*(findS-side3);
        float areas = (float) Math.sqrt(findS*findAll);
        return areas;
    }

    public int lines() {
        int lines = side1+side2+side3;
        return lines;
    }
}
